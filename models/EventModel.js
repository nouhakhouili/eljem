module.exports = (sequelize, type) => {
    return sequelize.define('Event', {
        id: {
            type: type.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        nom:type.STRING,
       description: type.STRING,
        date: type.STRING
    })
}