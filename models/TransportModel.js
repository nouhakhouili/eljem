module.exports = (sequelize, type) => {
    return sequelize.define('Transports', {
        id: {
            type: type.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        Catégorie: type.STRING,
        Adresse: type.STRING,
        Mobile: type.INTEGER,
        Fax: type.INTEGER,
        Email: type.STRING
    })
}