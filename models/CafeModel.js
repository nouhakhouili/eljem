module.exports = (sequelize, type) => {
    return sequelize.define('Cafes', {
        id: {
            type: type.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        Denomination: type.STRING,
        Type: type.STRING,
        Photo: type.STRING,
        Adresse: type.STRING,
        Menufourchette: type.STRING,
       // Mobile: type.INTEGER,
       // Fax: type.INTEGER,
        Email: type.STRING,
		 Commentaire:type.STRING,
		//Avie: type.DOUBLE,
    })
}