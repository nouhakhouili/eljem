const Sequelize = require('sequelize')

const UserModel = require('./UserModel')
const EventModel = require('./EventModel')
const FeedbackModel = require('./FeedbackModel')
const ArtisanatModel = require('./ArtisanatModel')
const CafeModel = require('./CafeModel')
const EvenementModel = require('./EvenementModel')
const HotelModel = require('./HotelModel')
const ParkingModel = require('./ParkingModel')
const RestaurantModel = require('./RestaurantModel')
const SiteModel = require('./SiteModel')
const AdminModel = require('./AdminModel')
const TransportModel = require('./TransportModel')
const sequelize = new Sequelize('bd1', 'root', '', {
  host: 'localhost',
  dialect: 'mysql',
  pool: {
    max: 10,
    min: 0,
    acquire: 30000,
    idle: 10000
  },
  operatorsAliases:false
})
const User = UserModel(sequelize, Sequelize)
const Feedback = FeedbackModel(sequelize, Sequelize)
const Event = EventModel(sequelize, Sequelize)
const Artisanat = ArtisanatModel(sequelize, Sequelize)
const Cafe = CafeModel(sequelize, Sequelize)
const Evenement = EvenementModel(sequelize, Sequelize)
const Hotel = HotelModel(sequelize, Sequelize)
const Parking = ParkingModel(sequelize, Sequelize)
const Restaurant = RestaurantModel(sequelize, Sequelize)
const Sitearcheologique = SiteModel(sequelize, Sequelize)
const Transport = TransportModel(sequelize, Sequelize)
const Admin = AdminModel(sequelize, Sequelize)
//force : true tafsa5 la9dim w tzid False t5alih w tzid 3lih
sequelize.sync({ force: false })
  .then(() => {
    console.log(`Database & tables created!`)
  })
module.exports = {
  User,
  Event,
  Feedback,
  Admin,
  Cafe
  
  }