module.exports = (sequelize, type) => {
    return sequelize.define('Admin', {
        id: {
            type: type.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        
        email: type.STRING,
        password: type.STRING
    })
}