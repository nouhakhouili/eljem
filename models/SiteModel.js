module.exports = (sequelize, type) => {
    return sequelize.define('sitearchéologiques', {
        id: {
            type: type.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        Nom: type.STRING,
        Type: type.STRING,
        Photo: type.STRING,
        Mobile: type.INTEGER,
        Fax: type.INTEGER,
        Email: type.STRING
    })
}