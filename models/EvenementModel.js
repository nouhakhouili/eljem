module.exports = (sequelize, type) => {
    return sequelize.define('Evenements', {
        id: {
            type: type.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        Nom: type.STRING,
        Periode: type.STRING,
        Photo: type.STRING,
        Description: type.STRING,
        Mobile: type.INTEGER,
        Fax: type.INTEGER,
        Email: type.STRING
        
    })
}