module.exports = (sequelize, type) => {
    return sequelize.define('Parkings', {
        id: {
            type: type.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        Type: type.STRING,
        Localisation: type.STRING
        
    })
}