module.exports = (sequelize, type) => {
    return sequelize.define('Artisanats', {
        id: {
            type: type.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        Nom: type.STRING,
        Categorie: type.STRING,
        Adresse: type.STRING,
        Mobil: type.INTEGER,
        Fax: type.INTEGER,
        Email : type.STRING,
        Commentaire:type.STRING,
		Avie: type.DOUBLE,
    })
}