module.exports = (sequelize, type) => {
    return sequelize.define('User', {
        id: {
            type: type.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        
        email: type.STRING,
        password: type.STRING,
		nom: type.STRING,
		prenom: type.STRING,
    })
}