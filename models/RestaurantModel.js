module.exports = (sequelize, type) => {
    return sequelize.define('Restaurants', {
        id: {
            type: type.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        Dénomination: type.STRING,
        Type: type.STRING,
        Adresse: type.STRING,
        Mobile: type.INTEGER,
        Fax: type.INTEGER,
        Email: type.STRING,
        Photo: type.STRING,
        Platconseillé: type.STRING,
        Menufourchette: type.STRING
        

    })
}