var {Admin} = require("../models/sequelize")
var bcrypt = require("bcrypt")
//json web token
//comme alert
var jwt = require("jsonwebtoken")
var randtoken = require("rand-token")
var tokenList = {};

module.exports = {
	
	delete: function(req, res){
    Admin.destroy({ 
        where: { id: req.params.id } 
    }).then(result => {
        res.status(200).json(result);
    });

    },
	
	update: function(req, res){
    Admin.update({
        email: req.body.email,
        password: req.body.password,
     
    },{ 
        where: { id: req.params.id } 
    }).then(result => {
        res.status(200).json(result);
    });

    },

	

    create: function (req, res, next) {


          console.log(req.body)
        var hashpassword = generateHash(req.body.password)
        Admin.create({
            // nom: req.body.nom,
            // prenom: req.body.prenom,
            email: req.body.email,
            password: hashpassword
            
        }).then(item => {
            res.send({status:"yes",msg:"create user"})
        })
    },
    // delete: function (req, res, next) {

        // Admin.destroy({
            // where: {
                // id: req.query.id
            // }
        // }).then(item => {


            // console.log(item)
            // res.send("okkk")
        // });

    // },
    getAll: function (req, res, next) {


        Admin.findAll({}).then(items => {

            res.send(items)
        })


    },
   // getUserById: function (req, res, next) {


    // Admin.findAll({
                // where:
                    // {
                        // id: req.query.id,
                       
 
                    // }

            // }
        // ).then(items => {


            // res.send(items)
        // })


    // },
	
	
	
	get: function(req, res){
    console.log('getting one admin');
    Admin.findAll({
                where:
                    {
                       id: req.params.id,
                   
                    }

            }
        ).then(admin => {
        console.log(admin);
		res.json(admin);
        })
       
    },
    /* another ways to do it
    Book.findOne({ where: {id: req.params.id} }).success(book => {
        console.log(book);
        res.json(book);
    }).error(err => {
        res.send('error has occured');
    });
    */

   
    // update: function (req, res, next) {


        // Admin.update(
            // req.body,
            // {where: {id: req.query.id}}
        // )
            // .then(result =>

                // res.send("ok update ")
            // )
            // .catch(err =>

                // res.send("ok error ")
            // )


    // },
    auth: function (req, res, next) {

         console.log(req.body)
         Admin.findAll({
                where:
                    {email: req.body.email
                    }
            }
        ).then(items => {

             console.log(items[0].password)
             console.log(req.body.password)


               
            if (bcrypt.compareSync(req.body.password, items[0].password)) {
                const token = jwt.sign({id: items[0].id},req.app.get("secretKey"), {expiresIn: '1h'});
                var refreshToken = randtoken.uid(256)
                tokenList[refreshToken] = items[0].id
                console.log({
                    status: "success",
                    message: "user found!!!",
                    data: {user: items[0], token: token, refreshToken: refreshToken}
                })

                res.json({
                    status: "success",
                    message: "user found!!!",
                    data: {user: items[0], token: token, refreshToken: refreshToken}
                });

            } else {
                res.json({status: "error", message: "Invalid email/password!!!", data: null});
            }


        }).catch(err => {

            console.log("err",err);

            res.send(err)
        })
    }

}

var generateHash = function (password) {

    return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);

};
