var express=require("express")
var EvenementsController=require("../controller/EvenementsController")
var router=express.Router();

router.post("/create",EvenementsController.create)
router.put("/update",EvenementsController.update)
router.delete("/delete",EvenementsController.delete)
router.get("/all",EvenementsController.getAll)
router.post("/BylastName",EvenementsController.getByLastName)

module.exports=router