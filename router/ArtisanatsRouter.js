var express = require("express")
var ArtisanatsController = require("../controller/ArtisanatsController")
var router = express.Router();

router.post("/create", ArtisanatsController.create)
router.put("/update", ArtisanatsController.update)
router.delete("/delete", ArtisanatsController.delete)
router.get("/all", ArtisanatsController.getAll)
router.post("/BylastName", ArtisanatsController.getByLastName)

module.exports = router