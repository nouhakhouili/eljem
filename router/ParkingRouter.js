var express=require("express")
var ParkingsController=require("../controller/ParkingsController")
var router=express.Router();

router.post("/create",ParkingsController.create)
router.put("/update",ParkingsController.update)
router.delete("/delete",ParkingsController.delete)
router.get("/all",ParkingsController.getAll)
router.post("/BylastName",ParkingsController.getByLastName)

module.exports=router