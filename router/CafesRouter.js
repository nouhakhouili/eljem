var express=require("express")
var CafesController=require("../controller/CafesController")
var router=express.Router();

router.post("/create",CafesController.create)
router.put("/update",CafesController.update)
router.delete("/delete",CafesController.delete)
router.get("/all",CafesController.getAll)
router.post("/BylastName",CafesController.getByLastName)

module.exports=router