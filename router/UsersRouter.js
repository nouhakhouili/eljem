var express=require("express")
var UsersController=require("../controller/UsersController")
var router=express.Router();

router.post("/create",UsersController.create)
router.put("/update/:id",UsersController.update)
router.delete("/delete/:id",UsersController.delete)
router.get("/all",UsersController.getAll)
router.get("/getId",UsersController.getUserById)
router.post("/auth",UsersController.auth)

module.exports=router