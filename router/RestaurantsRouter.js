var express=require("express")
var RestaurantsController=require("../controller/RestaurantsController")
var router=express.Router();

router.post("/create",RestaurantsController.create)
router.put("/update",RestaurantsController.update)
router.delete("/delete",RestaurantsController.delete)
router.get("/all",RestaurantsController.getAll)
router.post("/BylastName",RestaurantsController.getByLastName)

module.exports=router