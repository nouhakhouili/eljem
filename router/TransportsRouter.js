var express=require("express")
var TransportsController=require("../controller/TransportsController")
var router=express.Router();

router.post("/create",TransportsController.create)
router.put("/update",TransportsController.update)
router.delete("/delete",TransportsController.delete)
router.get("/all",TransportsController.getAll)
router.post("/BylastName",TransportsController.getByLastName)

module.exports=router