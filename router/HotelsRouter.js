var express=require("express")
var HotelsController=require("../controller/HotelsController")
//router n3aytoulou feserver
var router =express.Router();

router.post("/create",HotelsController.create)
router.put("/update",HotelsController.update)
router.delete("/delete",HotelsController.delete)
router.get("/all",HotelsController.getAll)
router.post("/BylastName",HotelsController.getByLastName)

//public router
//router.post("/create",HotelsController.create)
//router.post("/auth",HotelsController.auth)

//private router
//router.put("/update",VerifyToken,usersController.update)
//router.delete("/delete",VerifyToken,usersController.delete)
//router.get("/all",VerifyToken,usersController.getAll)
//router.post("/BylastName",VerifyToken,usersController.getByLastName)

module.exports=router

