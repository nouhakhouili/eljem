var express=require("express")
var AdminController=require("../controller/AdminController")
var router=express.Router();

router.post("/create",AdminController.create)
router.put("/update/:id",AdminController.update)
router.delete("/delete/:id",AdminController.delete)
router.get("/all",AdminController.getAll)
router.get("/getId/:id",AdminController.get)
router.post("/auth",AdminController.auth)

module.exports=router