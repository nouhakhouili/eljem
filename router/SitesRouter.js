var express=require("express")
var SitesController=require("../controller/SitesController")
var router=express.Router();

router.post("/create",SitesController.create)
router.put("/update",SitesController.update)
router.delete("/delete",SitesController.delete)
router.get("/all",SitesController.getAll)
router.post("/BylastName",SitesController.getByLastName)

module.exports=router