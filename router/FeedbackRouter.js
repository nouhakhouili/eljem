var express=require("express")
var feedbackController=require("../controller/feedbackController")
var router=express.Router();

router.post("/create",feedbackController.create)
router.put("/update",feedbackController.update)
router.delete("/delete",feedbackController.delete)
router.get("/all",feedbackController.getAll)


module.exports=router