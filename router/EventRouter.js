var express=require("express")
var EventController=require("../controller/EventController")
var router=express.Router();

router.post("/create",EventController.create)
router.put("/update",EventController.update)
router.delete("/delete",EventController.delete)
router.get("/all",EventController.getAll)


module.exports=router