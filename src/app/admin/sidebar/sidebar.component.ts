import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { UserService } from 'src/app/services/user.service';
import { Admin } from 'src/app/admin';


@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {
  id: number;
admin : Admin;
  constructor( private router:ActivatedRoute,private userService : UserService) { }

  ngOnInit() {
    this.router.params.subscribe((param) => {
      this.id = +param['id']
    })
    this.userService.getUserById(this.id)
      .subscribe((user) =>{ this.admin = user;
        console.log(user); })
  }
   }


