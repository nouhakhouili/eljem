import { Component, OnInit , Inject} from '@angular/core';
import {Router} from "@angular/router";

import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {first} from "rxjs/operators";
import { UserService } from 'src/app/services/user.service';
import { Admin } from 'src/app/admin';


@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.css']
})
export class EditUserComponent implements OnInit {
submitted = false;
  user: Admin;
  editForm: FormGroup;
  constructor(private formBuilder: FormBuilder,private router: Router, private apiService: UserService) { }

  ngOnInit() {
    let id = window.localStorage.getItem("editUserId");
    if(!id) {
      alert("Invalid action.")
      this.router.navigate(['/admin/listAdmin']);
      return;
    }
    this.editForm = this.formBuilder.group({
    
     email: ['', Validators.required],
      password: ['', Validators.required],
    confirmPassword: ['', [Validators.required]]
    });
    this.apiService.getUserById(+id)
      .subscribe( data => {
               this.user = data;
			  console.log(data);
      });
  }
  
  
  get f() {

     return this.editForm.controls;
  }

  go() {
    this.submitted=true;

    // stop here if form is invalid
    if (this.editForm.invalid) {
      return;
    }
    console.log(this.editForm.value)
    this.apiService.create(this.editForm.value)
    .subscribe( data => {
      this.router.navigate(['/admin/listAdmin']);
    });
}

  onSubmit() {
    this.apiService.update(this.user)

      .pipe(first())
      .subscribe(
        data => {
			  console.log(data)
          // if(data.status === 200) {
            // alert('User updated successfully.');
            // this.router.navigate(['/admin/user']);
          // }else {
            // alert(data.message);
          // }

         },
        error => {
          alert(error);
        });
  }

}
