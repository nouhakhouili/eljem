import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { EventService } from '../../../services/event.service';
import { Event } from '../../../event';

@Component({
  selector: 'app-add-event',
  templateUrl: './add-event.component.html',
  styleUrls: ['./add-event.component.css']
})
export class AddEventComponent implements OnInit {
registerForm: FormGroup;
  loading = false;
  submitted = false;

 events: Event;
  constructor( private formbuilder: FormBuilder,
    private router: Router,private eventservice:EventService
   ) {
  }

  ngOnInit() {
	   this.eventservice.getAll()
     .subscribe( data => {
       this.events = data;
   }); 

    this.registerForm = this.formbuilder.group({
      nom: ['', Validators.required],
    description: ['', Validators.required],
      //confirmPassword:['', MustMatch]
    date: ['', [Validators.required, Validators.minLength(10), Validators.maxLength(10)]],
      
    });
   

  }


  get f() {

     return this.registerForm.controls;
  }

  go() {
    this.submitted=true;

    // stop here if form is invalid
    if (this.registerForm.invalid) {
      return;
    }
    console.log(this.registerForm.value)
    this.eventservice.create(this.registerForm.value)
    .subscribe( data => {
      this.router.navigate(['/admin/event']);
    });
}


}
