import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { EventService } from '../../services/event.service';
import { Event } from '../../event';
@Component({
  selector: 'app-event',
  templateUrl: './event.component.html',
  styleUrls: ['./event.component.css']
})
export class EventComponent implements OnInit {
events: Event;

  constructor(private router: Router, private apiService: EventService) { }

  ngOnInit() {
    // if(!window.localStorage.getItem('token')) {
    //   this.router.navigate(['/admin/user']);
    //   return;
    // }
    this.apiService.getAll()
     .subscribe( data => {
       this.events = data;
   }); 
      ;
  }
}