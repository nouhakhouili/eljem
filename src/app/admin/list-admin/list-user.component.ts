import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import {UserService } from 'src/app/services/user.service';
import { Admin } from 'src/app/admin';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-list-user',
  templateUrl: './list-user.component.html',
  styleUrls: ['./list-user.component.css']
})
export class ListUserComponent implements OnInit {

  admins: Admin;
  ad: Admin[];

  constructor(private router: Router, private apiService: UserService) { }

  ngOnInit() {
    // if(!window.localStorage.getItem('token')) {
    //   this.router.navigate(['/admin/user']);
    //   return;
    // }
    this.apiService.getAll()
     .subscribe( data => {
       this.admins = data;
   }); 
      ;
  }
  spliceByElement(array: any[], element: any) {
    const index = array.indexOf(element);
    if (index > -1) {
      array.splice(index, 1);
    }
    return array;
  }

  delete(admin: Admin): void {
    this.apiService.delete(admin.id)
      // .subscribe( data => {
    // this.router.navigate(['/admin/listAdmin']);
    //console.log(data);
    this.spliceByElement(this.ad, admin);
        

    
	       
  };

  update(admin: Admin): void {
    window.localStorage.removeItem("editUserId");
    window.localStorage.setItem("editUserId", admin.id.toString());
    this.router.navigate(['/admin/edit-user']);
  };

  create(): void {
    this.router.navigate(['/register']);
  };
}

  