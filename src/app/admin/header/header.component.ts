import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { UserService } from 'src/app/services/user.service';
import { Admin } from 'src/app/admin';
import { AuthService } from '../../auth.service';


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  id: number;
admins: Admin;
email: String;

  constructor(private router: ActivatedRoute , private apiService: AuthService, private userservice : UserService) { }

  ngOnInit() {
  //   if(!window.localStorage.getItem('token')) {
  //     this.router.navigate(['/admin/user']);
  //     return;
  //   }
  //  // this.apiService.getUserById(1)
  //   // .subscribe( data => {
  //     // this.users["email"] = data;
  //  //}); 
  //    // ;
	 
	//   this.userservice.getUserById(4)
  //      .subscribe( data => {
	// 	 console.log(data.email);
		
  //    }); 
  

  this.router.params.subscribe((param) => {
    this.id = +param['id']
  })
  this.userservice.getUserById(this.id)
    .subscribe((user) =>{ this.admins = user;
      console.log(user); })
    
}

  
  
  logout(){	
    this.apiService.logout();
    //this.router.navigateByUrl('/login');
  }
  
} 
