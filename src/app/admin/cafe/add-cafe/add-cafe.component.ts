import { Cafe } from 'src/app/model/café';
import { CafeService } from 'src/app/services/cafe.service';
import { OnInit, Component } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-cafe',
  templateUrl: './add-cafe.component.html',
  styleUrls: ['./add-cafe.component.css']
})
export class AddCafeComponent implements OnInit {
	

 

 
  registerForm: FormGroup;
  loading = false;
  submitted = false;

 cafes: Cafe;
  constructor( private formbuilder: FormBuilder,
    private router: Router,private eventservice:CafeService
   ) {
  }

    ngOnInit() {
       this.eventservice.getAll()
       .subscribe( data => {
         this.cafes = data;
     }); 
  
      this.registerForm = this.formbuilder.group({
        Denomination: ['', Validators.required],
        Type: ['', Validators.required],
         Photo: ['', Validators.required],
        //confirmPassword:['', MustMatch]
        Adresse: ['', Validators.required],
        Menufourchette: ['', Validators.required],
        Email: ['',[Validators.required,Validators.email]],
        
      });
     
  
    }


  get f() {

     return this.registerForm.controls;
  }

  go() {
    this.submitted=true;

    // stop here if form is invalid
    if (this.registerForm.invalid) {
      return;
    }
    console.log(this.registerForm.value)
    this.eventservice.create(this.registerForm.value)
    .subscribe( data => {
      this.router.navigate(['/admin/cafe']);
    });
}


}

  