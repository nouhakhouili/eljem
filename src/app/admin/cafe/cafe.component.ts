import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { CafeService } from '../../services/cafe.service';
import { Cafe } from '../../model/café';

@Component({
  selector: 'app-cafe',
  templateUrl: './cafe.component.html',
  styleUrls: ['./cafe.component.css']
})
export class CafeComponent implements OnInit {
cafes: Cafe;

  constructor(private router: Router, private apiService: CafeService) { }

  ngOnInit() {
    // if(!window.localStorage.getItem('token')) {
    //   this.router.navigate(['/admin/user']);
    //   return;
    // }
    this.apiService.getAll()
     .subscribe( data => {
       this.cafes = data;
   }); 
      ;
  }
}