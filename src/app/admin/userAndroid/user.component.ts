import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserAndroidService } from 'src/app/services/user-android.service';
import { User } from 'src/app/model/user';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserAppComponent implements OnInit {
  users: User;

  constructor(private router: Router, private apiService: UserAndroidService) { }

  ngOnInit() {
    // if(!window.localStorage.getItem('token')) {
    //   this.router.navigate(['/admin/user']);
    //   return;
    // }
    this.apiService.getAll()
     .subscribe( data => {
       this.users = data;
   }); 
      
	 ;
  }

  delete(user: User): void {
    this.apiService.delete(user.id)
      .subscribe( data => {
        console.log(data);

      })
	        this.router.navigate(['/admin/user']);
  };
}