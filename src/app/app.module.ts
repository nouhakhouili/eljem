import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
//import { LoginComponent } from './login/login.component';
//import { RegisterComponent } from './register/register.component';
import { AdminComponent } from './admin/admin.component';
import { UserComponent } from './user/user.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';
import { HeaderComponent } from './admin/header/header.component';
import { SidebarComponent } from './admin/sidebar/sidebar.component';
import { FooterComponent } from './admin/footer/footer.component';
import { ErreurComponent } from './erreur/erreur.component';

import { LayoutComponent } from './admin/layout/layout.component';
import { InscritComponent } from './inscrit/inscrit.component';
import { EventComponent } from './admin/event/event.component';

import { AddEventComponent } from './admin/event/add-event/add-event.component';
import { CafeComponent } from './admin/cafe/cafe.component';
import { UserAppComponent } from './admin/userAndroid/user.component';

import { ListUserComponent } from './admin/list-admin/list-user.component';
import { AddCafeComponent } from './admin/cafe/add-cafe/add-cafe.component';
import { CompareValidatorDirective } from './shared/compare-validator.directive';
import { EditUserComponent } from './admin/edit-user/edit-user.component';
// import { ConfirmEqualValidatorDirective } from './custom-validator';




@NgModule({
  declarations: [
    AppComponent,
   
    AdminComponent,
    UserComponent,
    LoginComponent,
    RegisterComponent,
    HeaderComponent,
    SidebarComponent,
    FooterComponent,
    ErreurComponent,
    ListUserComponent,
    LayoutComponent,
    InscritComponent,
    EventComponent,
    AddEventComponent,
    CafeComponent,
    UserAppComponent,
    AddCafeComponent,
    CafeComponent,
    CompareValidatorDirective,
    EditUserComponent,
    // ConfirmEqualValidatorDirective
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule, ReactiveFormsModule, HttpClientModule
  ],
  providers: [
    // { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    // { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },

    // provider used to create fake backend
    //fakeBackendProvider
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
