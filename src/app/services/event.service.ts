import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Event } from '../event';
import {Observable} from "rxjs/index";
import { ApiResponse } from '../api-response';

@Injectable({
  providedIn: 'root'
})
export class EventService {
    constructor(private http: HttpClient) { }
    baseUrl="http://localhost:3001/event/";

   

      getAll() : Observable<Event> {
        return this.http.get<Event>("http://localhost:3001/event/all");
      }

    // getAll() {
    //     return this.http.get(`http://localhost:3001/event/all`);
    // }


    create(event: Event) {
        return this.http.post(`http://localhost:3001/event/create`, event);
    }

    update(id,event: Event) {
        return this.http.put(`http://localhost:3001/event/update` + id, event);
    }

    delete(id) {
        return this.http.delete(`http://localhost:3001/event/delete`+ id);
    }

    // updateUser(user: Event): Observable<ApiResponse> {
    //     return this.http.put<ApiResponse>(this.baseUrl+"/update" + user.id, user);
    //   }

    //   getUserById(id: number): Observable<ApiResponse> {
    //     return this.http.get<ApiResponse>(this.baseUrl + id);
    //   }
}