import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import {Observable, Subject} from "rxjs/index";
import { ApiResponse } from '../api-response';
import { Admin } from '../admin';
import { promise } from 'protractor';



@Injectable({ providedIn: 'root' })
export class UserService {
  private _refreshNeeded$ = new Subject<void>();

  get refreshNeeded$() {
    return this._refreshNeeded$;
  }
    constructor(private http: HttpClient) { }
    baseUrl="http://localhost:3001/user/";

    login(email, password) {

        return this.http.post("http://localhost:3001/admin/auth", {email: email, password: password});
      }

      getAll() : Observable<Admin> {
        return this.http.get<Admin>("http://localhost:3001/admin/all");
      }

   // getAll() {
    //    return this.http.get(`http://localhost:3001/user/all`);
    //}


    create(user: Admin) {
        return this.http.post(`http://localhost:3001/admin/create`, user);
    }

    // update(id,user: Admin) {
        // return this.http.put(`http://localhost:3001/admin/update` + id, user);
    // }

    // delete(id) {
        // return this.http.delete(`http://localhost:3001/admin/delete/`+ id);
		 // return this.http.delete(`http://localhost:3001/admin/delete/`+id)
    // }

    // updateUser(user: User): Observable<ApiResponse> {
    //     return this.http.put<ApiResponse>(this.baseUrl+"/update" + user.id, user);
	
	update(user: Admin): Observable<Admin> {
    return this.http.put<Admin>(`http://localhost:3001/admin/update` + user.id, user);
  }
    //   }
	
	delete(id: number): Observable<Admin> {
    return this.http.delete<Admin>(`http://localhost:3001/admin/delete/` + id);
  }

       getUserById(id: number): Observable<Admin> {
    return this.http.get<Admin>("http://localhost:3001/admin/getId/" + id);
  }
  
}