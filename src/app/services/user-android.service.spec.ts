import { TestBed } from '@angular/core/testing';

import { UserAndroidService } from './user-android.service';

describe('UserAndroidService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: UserAndroidService = TestBed.get(UserAndroidService);
    expect(service).toBeTruthy();
  });
});
