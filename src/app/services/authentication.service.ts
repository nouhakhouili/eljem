import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Admin } from '../admin';



@Injectable({ providedIn: 'root' })
export class AuthenticationService {
    private currentUserSubject: BehaviorSubject<Admin>;
    public currentUser: Observable<Admin>;

    constructor(private http: HttpClient) {
        this.currentUserSubject = new BehaviorSubject<Admin>(JSON.parse(localStorage.getItem('currentUser')));
        this.currentUser = this.currentUserSubject.asObservable();
    }

    public get currentUserValue(): Admin {
        return this.currentUserSubject.value;
    }

    login(email: string, password: string) {
        return this.http.post<any>("http://localhost:3001/admin/auth", { email, password })
            .pipe(map(admin => {
                // login successful if there's a jwt token in the response
                if (admin && admin.token) {
                    // store admin details and jwt token in local storage to keep admin logged in between page refreshes
                    localStorage.setItem('currentUser', JSON.stringify(admin));
                    this.currentUserSubject.next(admin);
                }

                return admin;
            }));
    }

    logout() {
        // remove admin from local storage to log admin out
        localStorage.removeItem('currentUser');
        this.currentUserSubject.next(null);
    }
}