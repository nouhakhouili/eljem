import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Cafe } from '../model/café';
import {Observable} from "rxjs/index";
import { ApiResponse } from '../api-response';

@Injectable({
  providedIn: 'root'
})
export class CafeService {
constructor(private http: HttpClient) { }
    baseUrl="http://localhost:3001/cafe/";

   

      getAll() : Observable<Cafe> {
        return this.http.get<Cafe>("http://localhost:3001/cafe/all");
      }

    // getAll() {
    //     return this.http.get(`http://localhost:3001/cafe/all`);
    // }


    create(cafe: Cafe) {
        return this.http.post(`http://localhost:3001/cafe/create`, cafe);
    }

    update(id,cafe: Cafe) {
        return this.http.put(`http://localhost:3001/cafe/update` + id, cafe);
    }

    delete(id) {
        return this.http.delete(`http://localhost:3001/cafe/delete`+ id);
    }

    // updateUser(user: Event): Observable<ApiResponse> {
    //     return this.http.put<ApiResponse>(this.baseUrl+"/update" + user.id, user);
    //   }

    //   getUserById(id: number): Observable<ApiResponse> {
    //     return this.http.get<ApiResponse>(this.baseUrl + id);
    //   }
}