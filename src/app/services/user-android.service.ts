import { Injectable } from '@angular/core';
import { User } from '../model/user';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UserAndroidService {
  constructor(private http: HttpClient) { }
  baseUrl="http://localhost:3001/user/";

  login(email, password) {

      return this.http.post("http://localhost:3001/user/auth", {email: email, password: password});
    }

    getAll() : Observable<User> {
      return this.http.get<User>("http://localhost:3001/user/all");
    }

 // getAll() {
  //    return this.http.get(`http://localhost:3001/user/all`);
  //}
update(user: User): Observable<User> {
    return this.http.put<User>(`http://localhost:3001/user/update` + user.id, user);
  }
    //   }
	
	delete(id: number): Observable<User> {
    return this.http.delete<User>(`http://localhost:3001/user/delete/` + id);
  }

  create(user: User) {
      return this.http.post(`http://localhost:3001/user/create`, user);
  }

  // update(id,user: User) {
      // return this.http.put(`http://localhost:3001/user/update` + id, user);
  // }

  // delete(id) {
      // return this.http.delete(`http://localhost:3001/user/delete/`+ id);
  // }

  // updateUser(user: User): Observable<ApiResponse> {
  //     return this.http.put<ApiResponse>(this.baseUrl+"/update" + user.id, user);
  //   }

   //  getUserById(id: number): Observable<User> {
     //  return this.http.get<User>("http://localhost:3001/user/getId/"+ id, {email: email});
     //}
}