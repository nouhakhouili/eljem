import { Directive, Input } from '@angular/core';
import { Validator, AbstractControl, ValidationErrors, NG_VALIDATORS } from '@angular/forms';
import { Subscription } from 'rxjs';

@Directive({
  selector: '[appCompareValidator]',
  providers: [{provide: NG_VALIDATORS, useExisting:CompareValidatorDirective,multi:true}]
})
export class CompareValidatorDirective implements Validator{
  @Input('appCompareValidator') controlNameToCompare: string;
  

  constructor() { }

  validate(c:AbstractControl): ValidationErrors|null{
    const ControlToCompare = c.root.get(this.controlNameToCompare);
    if (ControlToCompare){
      const subscription : Subscription = ControlToCompare.valueChanges.subscribe(()=>{

        c.updateValueAndValidity();
        subscription.unsubscribe();
      })
    }
    return ControlToCompare && ControlToCompare.value !== c.value ? {'appCompareValidator':true}:null
  }

}
