import { Directive, Input } from "@angular/core";
import { NG_VALIDATORS, AbstractControl ,Validator, FormGroup, ValidationErrors} from '@angular/forms';

/*import { AbstractControl } from '@angular/forms';*/

// import { AbstractControl } from '@angular/forms';

// export class CustomValidator {
//     static passwordMatchValidator(control: AbstractControl) {
//         const password: string = control.get('password').value; // get password from our password form control
//         const confirmPassword: string = control.get('confirmPassword').value; // get password from our confirmPassword form control
//         // compare is the password math
//         if (password !== confirmPassword) {
//           // if they don't match, set an error in our confirmPassword form control
//           control.get('confirmPassword').setErrors({ NoPassswordMatch: true });
//         }
//       }
// }


// import { Directive, Input } from '@angular/core';
// import { NG_VALIDATORS, Validator, ValidationErrors, FormGroup } from '@angular/forms';
// import { MustMatch } from './must-match';

//  import { MustMatch } from './must-match';

// @Directive({
//     selector: '[mustMatch]',
//     providers: [{ provide: NG_VALIDATORS, useExisting: MustMatchDirective, multi: true }]
// })
// export class MustMatchDirective implements Validator {
//     @Input('mustMatch') mustMatch: string[] = [];

//     validate(formGroup: FormGroup): ValidationErrors {
//         return MustMatch(this.mustMatch[0], this.mustMatch[1])(formGroup);
//     }
// }
// // 


 export class ConfirmPasswordValidator {
  static MatchPassword(control: AbstractControl) {
     let password = control.get('password').value;

     let confirmpassword = control.get('confirmpassword').value;

      if(password != confirmpassword) {
          control.get('confirmpassword').setErrors( {ConfirmPassword: true} );
      } else {
          return null
      }
  }
} 

/*@Directive({
    selector:'[appConfirmEqualValidator]',
    providers:[{
        provide:NG_VALIDATORS,
        useExisting:ConfirmEqualValidatorDirective,
        multi: true
    }]
    export class ConfirmEqualValidatorDirective implements Validator{
        @Input() appConfirmEqualValidator:string;
        validate (control:AbstractControl):{[key:string]:any } | null{
            const controlToCompare= control.parent.get(this.appConfirmEqualValidator);
            if (controlToCompare && controlToCompare.value !== control.value)
            return {'notEqual':true};

        }
        return null;
     }
}) */