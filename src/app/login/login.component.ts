import {Component, OnInit} from '@angular/core';
import {Router,ActivatedRoute} from "@angular/router";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
//mport { AuthenticationService } from '../services/authentication.service';
import { first } from 'rxjs/operators';
import { AlertService } from '../services/alert.service';
import { UserService } from '../services/user.service';
import { AuthService } from '../auth.service';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loading = false;
  submitted = false;
  returnUrl: string;
  loginForm: FormGroup;
  

  constructor(private  router: Router,private userService: AuthService, private route: ActivatedRoute, private formbuilder: FormBuilder) { }

  ngOnInit() {


    this.loginForm = this.formbuilder.group({

      email: ['',[Validators.required,Validators.email]],
       password: ['',[Validators.required,Validators.minLength(8)]]
    });
    //  // get return url from route parameters or default to '/'
    //  this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/admin';
  }
 get f(){
    return this.loginForm.controls;
 }
 go(){
    this.submitted=true;
    if(this.loginForm.invalid){
      return this.loginForm;
    }
    

   this.userService.login(this.loginForm.value);
    this.router.navigateByUrl('/admin');
  }
}
   // this.userService.login(this.loginForm.value["email"],this.loginForm.value["password"]).subscribe(res=>{


     // console.log(res["status"]);
     // console.log(JSON.parse(JSON.stringify(res["data"])).token);
     // console.log(JSON.parse(JSON.stringify(res["data"])).user);
     // console.log(JSON.parse(JSON.stringify(JSON.parse(JSON.stringify(res["data"])).user)).email);
     // this.router.navigate(["/admin"])*/
   // })
 // }

// }


// export class LoginComponent implements OnInit {

//   loginForm: FormGroup;
//   submitted = false;
//   loading = false;
//   returnUrl: string;


//   constructor(private  router: Router, private authenticationService: AuthenticationService,  private alertService: AlertService ,private formbuilder: FormBuilder) {
//      // redirect to home if already logged in
//      if (this.authenticationService.currentUserValue) { 
//       this.router.navigate(['/admin']);
//   }
//   }

//   ngOnInit() {

//     this.loginForm = this.formbuilder.group({
//       email: ['', [Validators.required, Validators.email]],
//       password: ['', [Validators.required, Validators.minLength(8)]]
      
//     });

//   }


//   get f() {

//      return this.loginForm.controls;
//   }

//   go() {

//     this.submitted=true;

//     // stop here if form is invalid
//     if (this.loginForm.invalid) {
//       return;
//     }


//     this.loading = true;
//     this.authenticationService.login(this.f.email.value, this.f.password.value)
//         .pipe(first())
//         .subscribe(
//             data => {
//                 this.router.navigate([this.returnUrl]);
//             },
//             error => {
//                  this.alertService.error(error);
//                 this.loading = false;
//             });

    

//     //  console.log(this.loginForm.value["email"]);
//     //  console.log(this.loginForm.value["password"]);
//    /// alert('SUCCESS!! :-)');


//   //  this.router.navigate(["admin"]);

//    // console.log("okkkkkk go")
//   }
// }
