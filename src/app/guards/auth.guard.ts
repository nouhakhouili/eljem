import { Injectable } from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree} from '@angular/router';
import { AuthService } from '../auth.service';
//import { AuthService } from '../auth.service';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})




export class AuthGuard implements CanActivate {
  constructor(private authService: AuthService){}
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {

      return this.authService.isLoggedIn();

  }
}


// export class AuthGuard implements CanActivate {

     // constructor(
        // private router: Router,authservice:AuthService
     
    // ) {}
	
// canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    // if (localStorage.getItem('token')) {
      // return true;
    // }
    // else {
      // this.router.navigate(["/login"])

      // return false;
    // }
  // }

// }
    // canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        // const currentUser = this.authenticationService.isLoggedIn;
        // if (currentUser) {
            // authorised so return true
            // this.router.navigate(['/admin']);
            // return true;
        // }

        // not logged in so redirect to login page with the return url
 


//   path: ActivatedRouteSnapshot[];
//   route: ActivatedRouteSnapshot;


//   constructor(private router: Router) {
//   }

//   canActivate() {
//     if (localStorage.getItem('var') === '1') {
//       return true;
//     }
//     else {
//       this.router.navigate(["login"])

//       return false;
//     }
//   }

// }
