import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
//import { MustMatch } from '../must-match';
import { ConfirmPasswordValidator } from '../custom-validator';
import { AuthenticationService } from '../services/authentication.service';

import { AlertService } from '../services/alert.service';
import { UserService } from '../services/user.service';
import {checkPasswordMatch } from '../services/validation.service';
import { CompareValidatorDirective } from '../shared/compare-validator.directive';

// import { first } from 'rxjs/operators';

// import { AlertService, UserService, AuthenticationService } from '../_services';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})

export class RegisterComponent implements OnInit {

  registerForm: FormGroup;
  loading = false;
  submitted = false;


  constructor( private formbuilder: FormBuilder,
    private router: Router,private userservice:UserService
   ) {
  }

  ngOnInit() {

    this.registerForm = this.formbuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(8)]],
      //confirmPassword:['', MustMatch]
     confirmPassword: ['', [Validators.required]]
    });
   

  }


  get f() {

     return this.registerForm.controls;
  }

  go() {
    this.submitted=true;

    // stop here if form is invalid
    if (this.registerForm.invalid) {
      return;
    }
    console.log(this.registerForm.value)
    this.userservice.create(this.registerForm.value)
    .subscribe( data => {
      this.router.navigate(['/admin/listAdmin']);
    });
}

}
    //  console.log(this.registerForm.value["email"]);
    //  console.log(this.registerForm.value["password"]);
   /// alert('SUCCESS!! :-)');


  

   // console.log("okkkkkk go")
 

// export class RegisterComponent implements OnInit {
//   registerForm: FormGroup;
//   loading = false;
//   submitted = false;

//   constructor(
//       private formBuilder: FormBuilder,
//       private router: Router,
//       private authenticationService: AuthenticationService,
//       private userService: UserService,
//       private alertService: AlertService
//   ) { 
//       // redirect to home if already logged in
//       if (this.authenticationService.currentUserValue) { 
//           this.router.navigate(['/']);
//       }
//   }

//   ngOnInit() {
//       this.registerForm = this.formBuilder.group({
//           
//           email: ['', Validators.required],
//           password: ['', [Validators.required, Validators.minLength(6)]]
//       });
//   }

//   // convenience getter for easy access to form fields
//   get f() { return this.registerForm.controls; }

//   onSubmit() {
//       this.submitted = true;

//       // stop here if form is invalid
//       if (this.registerForm.invalid) {
//           return;
//       }

//       this.loading = true;
//       this.userService.register(this.registerForm.value)
//           .pipe(first())
//           .subscribe(
//               data => {
//                   this.alertService.success('Registration successful', true);
//                   this.router.navigate(['/login']);
//               },
//               error => {
//                   this.alertService.error(error);
//                   this.loading = false;
//               });
//   }
// }