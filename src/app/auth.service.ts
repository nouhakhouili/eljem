import { Injectable } from '@angular/core';
import { Admin } from './admin';
import {Router,ActivatedRoute} from "@angular/router";
@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private  router: Router) { }

  public login(userInfo: Admin){
    localStorage.setItem('ACCESS_TOKEN', "access_token");
  }

  public isLoggedIn(){
    return localStorage.getItem('ACCESS_TOKEN') !== null;

  }

  public logout(){
    localStorage.removeItem('ACCESS_TOKEN');
	
  }
}