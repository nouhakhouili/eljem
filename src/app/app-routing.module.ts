import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import {RegisterComponent} from './register/register.component';
import {AdminComponent} from './admin/admin.component';
import {UserComponent} from './user/user.component';
import {AuthGuard} from './guards/auth.guard';
import { ErreurComponent } from './erreur/erreur.component';

import { LayoutComponent } from './admin/layout/layout.component';
import { EventComponent } from './admin/event/event.component';
import { AddEventComponent } from './admin/event/add-event/add-event.component';
import { UserAppComponent } from './admin/userAndroid/user.component';
import { ListUserComponent } from './admin/list-admin/list-user.component';

import { CafeComponent } from './admin/cafe/cafe.component';
import { AddCafeComponent } from './admin/cafe/add-cafe/add-cafe.component';
import { EditUserComponent } from './admin/edit-user/edit-user.component';

const routes: Routes = [{path:"login",component:LoginComponent},
{path: "register", component: RegisterComponent},
{path: "admin", component: AdminComponent ,children:[{path:'listAdmin',component:ListUserComponent},
{path: "", component: LayoutComponent},
{path:'user',component:UserAppComponent},
{path: "event", component: EventComponent},
{path: "add-event", component: AddEventComponent},
{path: "add-cafe", component: AddCafeComponent},
{path: "cafe", component: CafeComponent},
{path: "edit-user", component: EditUserComponent},
/*,canActivate:[AuthGuard]]},*/
	], canActivate:[AuthGuard] },
{path: "", component: UserComponent},
{path: "**", component: ErreurComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
