var express = require("express")
var cors=require("cors")
//fs= fil system
const fs=require("fs")
//morgan : mode developpement
var logger=require('morgan');
//multer pour l'image
const multer = require('multer');



const upload=multer({dest: __dirname + '/uploads/images'});

var bodyParser=require("body-parser")

var UsersRouter=require("./router/UsersRouter")
var EventRouter=require("./router/EventRouter")
var FeedbackRouter=require("./router/FeedbackRouter")
var ArtisanatsRouter=require("./router/ArtisanatsRouter")
var CafesRouter=require("./router/CafesRouter")
var EvenemetsRouter=require("./router/EvenemetsRouter")
var HotelsRouter=require("./router/HotelsRouter")
var ParkingRouter=require("./router/ParkingRouter")
var RestaurantsRouter=require("./router/RestaurantsRouter")
var SitesRoute=require("./router/SitesRouter")
var TransportsRouter=require("./router/TransportsRouter")
var AdminRouter=require("./router/AdminRouter")
//bodyParser : pour envoyer les donnes => tnajem ta9rahem ken bel bodyParser 
//on a 3 regle pour le bodyParser var app= express()
var app=express();
//obligatoire

app.set("secretKey", "narjes")
app.use(bodyParser.json())
app.use(cors());
app.use(logger("dev"))
app.use("/user",UsersRouter)
app.use("/event",EventRouter)
app.use("/feedback",FeedbackRouter)
app.use("/artisanat",ArtisanatsRouter)
app.use("/cafe",CafesRouter)
app.use("/evenement",EvenemetsRouter)
app.use("/hotel",HotelsRouter)
app.use("/parking",ParkingRouter)
app.use("/restaurant",RestaurantsRouter)
app.use("/sitearcheologique",SitesRoute)
app.use("/transport",TransportsRouter)
app.use("/admin",AdminRouter)

app.get("/",function (req,res){
    res.send("welcom to my server")
})
app.post('/file_upload', upload.single("file"), function (req, res) {

    var file = __dirname + '/uploads/images' + req.file.originalname;

    fs.readFile(req.file.path, function (err, data) {


        fs.writeFile(file, data, function (err) {
            if (err) {
                console.error(err);
                response = {
                    message: 'Sorry, file couldn\'t be uploaded.',
                    filename: req.file.originalname
                };
            } else {
                response = {
                    message: 'File uploaded successfully',
                    filename: req.file.originalname
                };
            }
            res.end(JSON.stringify(response));
        });
    });
})

app.get("/images/:img",function (req,res){
    res.sendFile(__dirname+"/uploads/"+req.params.img)

}) 


app.get("/home",function (req,res){
    res.send("welcom to my Home")
})


app.listen(3001,function(){
    
    console.log("start");

})